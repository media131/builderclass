/**
 * include scanner for user input
 */
import java.util.Scanner;
public class Builder 
{
	public static void main(String[] args) 
	{
		/**
		 * scanner initialized
		 */
		Scanner keyboard = new Scanner(System.in);
		/**
		 * user is prompted to type this string exactly as shown
		 */
		String answer = " Yes, ";
		/**
		 * java is fun string initialized and is given the length
		 */
		String javaFun = "Java is fun!";
		System.out.println("String length: "+javaFun.length());
		/**
		 * object is made to append and insert
		 */
		StringBuilder newJavaFun = new StringBuilder(javaFun);
		/**
		 * capacity given
		 */
		System.out.println("Capacity: "+newJavaFun.capacity());
		/**
		 * object appended
		 */
		newJavaFun.append("I Love It!");
		/**
		 * prompts user to enter " Yes, " exactly including space		
		 */
		System.out.println("Enter the string\n\" Yes, \"");
		System.out.println("Exactly as it says.");
		String input = keyboard.nextLine();
		/**
		 * While loop keeps asking user to enter it exactly until answer is correct
		 */
		while (!(input.equals(answer)))
		{
			System.out.println("Wrong.");
			System.out.println("Enter the string\n\" Yes, \"");
			System.out.println("Exactly as it says.");
			input = keyboard.nextLine();
		}
		/**
		 * input is inserted between the two strings at the length value of the string
		 */
		newJavaFun.insert(javaFun.length(),input);
		System.out.println(newJavaFun);		
	}
}
